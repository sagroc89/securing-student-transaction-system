const fs = require('fs'); // For file processing
const aesjs = require('aes-js'); // AES package for encryption
const pbkdf2 = require('pbkdf2'); // To convert a password to a cobstant length key for AES encryption


/*
This method is to decrypt any data using AES decryption based on the users password
*/
function decrypt(password, data_to_decrypt){

	var pass_key_128_1 = pbkdf2.pbkdf2Sync(password, 'salt', 1, 128 / 8, 'sha512');
	var aesCtr1 = new aesjs.ModeOfOperation.ctr(pass_key_128_1, new aesjs.Counter(5));
	var decryptedBytes = aesCtr1.decrypt(data_to_decrypt);
	 
	// Convert our bytes back into text
	var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
	console.log("after decrypt");
	console.log(decryptedText);

	return decryptedText;
}

exports.decrypt = decrypt;