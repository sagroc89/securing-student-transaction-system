
var express = require('express');
var bodyParser = require('body-parser'); //parsing JSON and url-encoded data
var path = require('path');
var multer = require('multer'); //parsing multipart/form data
var emailsender = require('./emailsender');
var encrypt = require('./encrypt_data');
var decrypt = require('./decrypt_data');
var upload = multer();
var middleware = express();
middleware.set('view engine', 'pug');
middleware.set('views','./views');


const port = 3000;

middleware.use(function(req, res, next){
	console.log('Time: ', Date.now());
	next();
});

middleware.use(bodyParser.json());
middleware.use(bodyParser.urlencoded({extended: true}));
middleware.use(upload.array());
middleware.use(express.static(path.join(__dirname, 'public')));


middleware.get('/login', function(req, res){
	res.sendfile('public/login_page.html');
	console.log("SENT THE LOGIN PAGE");
});

var student_id = null;
var password = null;
var rand_otp = null;
var login_validation = false;
let email = "";


middleware.post('/login', function(req, res){

	login_data_received = req.body;
	student_id = login_data_received["studid"];
	password = login_data_received["pass"];

	password_in_db = "abcd1234"; // getPassword(student_id)call Rasika's method to get password

	if(password==password_in_db){// Send the verifcation code
		 email = "sagroc89@gmail.com"; // getEmail(student_id)call Rasika's method to get password
		 rand_otp = Math.floor((Math.random() * 90000) + 10000); // Getting random OTP
		 login_validation = true;
		 emailsender.send_code(rand_otp, email); // sending the OTP
		 res.sendfile('public/front_end_verify_page.html'); // Sending the verifying page for entering OTP
	}
	else{
		login_validation = false;
		// Send a message to enter correct password
		res.send("Enter proper login credentials!!");
	}
});

// middleware.get('/verify', function(req, res){
// 		res.sendfile('public/front_end_verify_page.html');
// 		console.log("SENT THE VERify PAGE");
	
// });
// POST route tofor verification of OTP
middleware.post('/verify', function(req, res){
	var req_data = req.body;
	opt_entered = req_data['otp'];
	console.log("From verify post");
	console.log(password);

	if(opt_entered==rand_otp){
		var full_encrypted_data_from_db =  {'email':"Sanket@gmail.com"};//getFullData(stud_id) Rasika's code ;
		// write decryption logic to decrypt data based on password and show it in frontend

		var stu_name = "Sanket Ag"; 
		var stu_id = "982992";
		var amt_due = 8000;
		var fin_aid = 20;
		var phone = '585-219-7777'; // decrypt.decrypt(password, full_encrypted_data_from_db['phone']);
		var acc_hold_name = "Sanket"; 
		var b_name = "BOFA"; 
		var acc_num = ""; // decrypt.decrypt(password, full_encrypted_data_from_db['acc_num']);
		var rou_num = ""; // decrypt.decrypt(password, full_encrypted_data_from_db['routing']);
		var amn_to_be_paid=0.0;

	  res.render('transaction_page', {
      stu_name: stu_name,
      stu_id: stu_id,
      email: full_encrypted_data_from_db['email'],
      amt_due: amt_due,
      fin_aid: fin_aid,
      phone: phone,
      acc_hold_name: acc_hold_name,
      b_name: b_name,
      acc_num: acc_num,
      rou_num: rou_num,
      amn_to_be_paid: amn_to_be_paid
   });
	}
	else{
		res.send("Incorrect OTP!!");
	}
});

middleware.post('/transact', function(req, res){
   res.send("Transaction Succesfull!!");
   console.log("Transaction done!!");
   console.log(req.body);
   // req.body will be sent to Rasika's code for updating the db with the amount paid
   // payThe Amount(req.body);

});



middleware.listen(port);

console.log("server started on port "+port);

module.exports = middleware;