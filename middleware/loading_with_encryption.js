
// console.log(con)
const {MongoClient, ObjectID} = require('mongodb');
const fs = require('fs'); // For file processing
const aesjs = require('aes-js'); // AES package for encryption
const pbkdf2 = require('pbkdf2'); // To convert a password to a cobstant length key for AES encryption

var transaction_file = JSON.parse(fs.readFileSync('../data/data.json','utf-8'));
var password_file = JSON.parse(fs.readFileSync('../data/passwords.json','utf-8'));
console.log(password_file[0]["sagroc89@gmail.com"]);


// for(var key in transaction_file[0]){
//   stud_trans = transaction_file[0][key];
//   password = password_file[0][stud_trans["email"]][0];
//   var pass_key_128 = pbkdf2.pbkdf2Sync(password, 'salt', 1, 128 / 8, 'sha512');
//   var aesCtr = new aesjs.ModeOfOperation.ctr(pass_key_128, new aesjs.Counter(5));

// // Applying Symmetric encryption
//   stud_trans["phone"] = aesCtr.encrypt(aesjs.utils.utf8.toBytes(stud_trans["phone"]));
//   stud_trans["address"] = aesCtr.encrypt(aesjs.utils.utf8.toBytes(stud_trans["address"]));
//   stud_trans["acc_num"] = aesCtr.encrypt(aesjs.utils.utf8.toBytes(stud_trans["acc_num"]));
//   stud_trans["routing"] = aesCtr.encrypt(aesjs.utils.utf8.toBytes(stud_trans["routing"]));

//   console.log(stud_trans["acc_num"])

// // var pass_key_128_1 = pbkdf2.pbkdf2Sync(password, 'salt', 1, 128 / 8, 'sha512');
// //   var aesCtr1 = new aesjs.ModeOfOperation.ctr(pass_key_128_1, new aesjs.Counter(5));
// // var decryptedBytes = aesCtr1.decrypt(encryptedBytes);
 
// // // Convert our bytes back into text
// // var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
// // console.log("after decrypt");
// // console.log(decryptedText);
// }

console.log(transaction_file);

MongoClient.connect('mongodb://localhost:27017/student_transaction_system', (err, client) => {
  if (err) {
    return console.log('Unable to connect to the MongoDB serve');
  }
  console.log('Connected to the MongoDB server');
  const db = client.db('student_transaction_system');

  // Inserting transaction data to mongodb
  // db.collection('transaction_data').insertMany(transaction_file, (err, result) => {
  //   if (err) {
  //     return console.log('Unable to insert todo', err);
  //   }

  //   console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
  // });

  // Inserting passwords to mongodb
  db.collection('passwords').insertMany(password_file, (err, result) => {
    if (err) {
      return console.log('Unable to insert todo', err);
    }

    console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
  });

  client.close();
});




