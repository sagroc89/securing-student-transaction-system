const fs = require('fs'); // For file processing
const aesjs = require('aes-js'); // AES package for encryption
const pbkdf2 = require('pbkdf2'); // To convert a password to a cobstant length key for AES encryption

/*
This method is to encrypt any data using AES encryption based on the users password
*/
function encrypt(password, data_to_encrypt){

	var pass_key_128 = pbkdf2.pbkdf2Sync(password, 'salt', 1, 128 / 8, 'sha512');
  	var aesCtr = new aesjs.ModeOfOperation.ctr(pass_key_128, new aesjs.Counter(5));
  	encrypted_data = aesCtr.encrypt(aesjs.utils.utf8.toBytes(data_to_encrypt));

  	return encrypted_data;
}

exports.encrypt = encrypt;