import names
import pickle
import barnum
import random
import json
import secrets
import string

student_transaction_data = {}
all_student_info = {}


def generating_data():
    student_count = 0
    total_amount = 10000
    student_ids = set()
    student_account_num = set()
    passwords = {}
    email_mapped_w_id = {}

    college_names = ["College of Art and Design", "Saunders College of Business",
                     "B. Thomas Golisano College of Computing and Information Sciences",
                     "Kate Gleason College of Engineering", "College of Engineering Technology",
                     "College of Health Sciences and Technology",
                     "College of Liberal Arts", "National Technical Institute for the Deaf", "College of Science",
                     "School of Individualized Study",
                     "Golisano Institute for Sustainability"]
    bank_names = ["bofa", "advantage", "keys", "wells fargo", "chase"]
    bank_routing_num = {"bofa": 225289120, "advantage": 567824109, "keys": 299292926,
                        "wells fargo": 198260715, "chase": 673521890}

    alphabets = string.ascii_letters + string.digits
    acc_num_1 = None
    while student_count < 10000:
        password = ''.join(secrets.choice(alphabets) for i in range(8))
        name = barnum.create_name()
        id = barnum.create_cc_number(None, 10)
        while True:
            id = barnum.create_cc_number(None, 10)[1][0]
            if id not in student_ids:
                break

        phone = barnum.create_phone('14623')
        address = barnum.create_street()
        email_id = barnum.create_email()
        college_name = college_names[random.randint(0, 9)]
        amount_paid = 0
        payment_timestamp = -9999
        financial_aid = 10 * random.randint(0, 4)
        amount_due = total_amount * (1 - (financial_aid / 100))
        account_holder_name = name
        bank_name = bank_names[random.randint(0, 4)]
        account_number = barnum.create_cc_number(None, 12)
        acc_num_1 = account_number
        while True:
            account_number = barnum.create_cc_number(None, 12)[1][0]
            if account_number not in student_account_num:
                break
        routing_num = bank_routing_num[bank_name]

        student_detail = {"name": name, "phone": phone, "address": address, "email": email_id,
                          "college": college_name, "amnt_paid": amount_paid, "timestamp": payment_timestamp,
                          "f_aid": financial_aid, "amnt_due": amount_due, "acc_holder_name": account_holder_name,
                          "bank_name": bank_name, "acc_num": int(account_number), "routing": routing_num}



        student_transaction_data[id] = student_detail
        passwords[id] = password



        student_count += 1
        print(student_count)

    my_detail = {"name": "Sanket Agarwal", "phone": "585-219-7327", "address": "ROchester",
                 "email": "sagroc89@gmail.com",
                 "college": "RIT", "amnt_paid": 0, "timestamp": payment_timestamp,
                 "f_aid": 30, "amnt_due": total_amount * (1 - (30 / 100)), "acc_holder_name": "Sanket Agarwal",
                 "bank_name": bank_names[0], "acc_num": 123478292012, "routing": bank_routing_num[bank_names[0]]}
    student_transaction_data[123456] = my_detail
    passwords[123456] = "xyz123abc"


    with open('data.json', 'w') as fp:
        json.dump([student_transaction_data], fp)

    with open('passwords.json', 'w') as fp:
        json.dump([passwords], fp)

    print(student_transaction_data)

if __name__ == '__main__':
    generating_data()
    data = None
    # with open('student_transaction_data.pickle', 'rb') as handle:
    #     data = pickle.load(handle)
    #
    # print(len(data))

    # for k, val in data.items():
    #     print(k)
    #     print(val)
