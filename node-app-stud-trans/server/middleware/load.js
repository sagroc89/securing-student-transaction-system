// var file = require('./studntdata.json');
// console.log(con)
const {MongoClient, ObjectID} = require('mongodb');
const fs = require('fs');

var file = JSON.parse(fs.readFileSync('./data.json','utf-8'));
console.log(file);

MongoClient.connect('mongodb://localhost:27017/studentfeepayment', (err, client) => {
  if (err) {
    return console.log('Unable to connect to the MongoDB serve');
  }
  console.log('Connected to the MongoDB server');
  const db = client.db('studentfeepayment');

  db.collection('Student Transaction').insertMany(file, (err, result) => {
    if (err) {
      return console.log('Unable to insert todo', err);
    }

    console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
  });

  client.close();
});
