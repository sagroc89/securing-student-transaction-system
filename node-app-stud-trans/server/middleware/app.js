const express = require("express");
require('../db/mongoose')
const User = require('../db/models/user')
const Trans = require('../db/models/transactions')

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json())

//Get all the transactions
app.get('/transaction', (req,res) => {
  Trans.find({}).then((trans) => {
    res.send(trans)
  }).catch(() => {
    res.status(500).send()
  })
})

//Get individual transactions by id
app.get('/transaction/:id', (req,res) => {
  const uid = req.params.id

  Trans.findById(uid).then((trans) => {
    if(!trans) {
      res.status(404).send()
    }

    res.send(trans)
  }).catch((e) => {
    res.status(500).send()
  })
  console.log(req.params)
})

//Creating a new user
app.post('/users',(req, res) => {
  const user = new User(req.body)
  // console.log(req.body)
  // res.send('testing!')
  user.save().then(() => {
    res.status(201).send(user)
  }).catch((e) => {
    res.status(400).send(e)
  })
});

//Creating new transaction
app.post('/transaction', (req,res) => {
  var trans = new Trans(req.body)
  trans.save().then(() => {
    res.status(201).send(trans)
  }).catch((e) => {
    res.status(400).send(e)
  })
})

app.listen(port, () => {
 console.log("Server listening on port " + port);
});
