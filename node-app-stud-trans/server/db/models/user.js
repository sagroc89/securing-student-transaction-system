const validator = require('validator');
const mongoose = require('mongoose');

const Test_Users = mongoose.model('Test_Users', {
  uid: {
    type: Number,
    required: true
  },
  name: { //set up validation or custom validation for each property
    first_name:{
      type: String,
      trim: true,
      required: true
    },
    last_name: {
      type: String,
      trim: true,
      required: true
    }
  },
  phone: {
    type: String,
    required: true
  },
  address:{
    type: String,
  },
  email:{
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if(!validator.isEmail(value)) {
        throw new Error('Email is invalid')
      }
    }
  },
  college: {
    type: String,
    required: true,
    trim: true
  },
  amnt_due: {
    type: Number,
    trim: true,
    required: true
  },
  password: {}
})

module.exports = Test_Users;
