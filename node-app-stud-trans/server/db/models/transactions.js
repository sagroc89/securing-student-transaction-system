const validator = require('validator');
const mongoose = require('mongoose');

const Test_transactions = mongoose.model('Test-transactions', {
  uid: {
    type: Number,
    required: true
  },
  amnt_paid: {
    type: Number,
    required: true,
    trim: true
  },
  timestamp: {
    type: Date,
    default: Date.now
  },
  f_aid: {
    type: Number,
    default: 0,
    trim: true
  },
  acc_holder_name: {
    first_name:{
      type: String,
      trim: true,
      required: true
    },
    last_name: {
      type: String,
      trim: true,
      required: true
    }
  },
  bank_name: {
    type: String,
    trim: true,
    required: true
  },
  acc_num: {
    type: Number,
    trim: true,
    required: true
  },
  routing: {
    type: Number,
    trim: true,
    required: true
  },
});

module.exports = Test_transactions
