const mongoose = require('mongoose');

//indicates that we use built in promise
//mongoose.Promise = global.Promise;
//connecting to db
// mongoose.connect('mongodb://localhost:27017/DBname');
mongoose.connect('mongodb://localhost:27017/studentfeepayment', {
  userNewUrlParser: true,
  userCreateIndex: true
});

// module.exports = {mongoose};



// const User = mongoose.model('User',{
//   name: {
//     type: String,
//     required: true
//   },
//   age: {
//     type: Number
//   }
// })

// const me = new User({
//   name: 'Rasika',
//   age: 23
// })
//
// me.save().then(() => {
//   console.log(me)
// }).catch((error) => {
//   console.log('Error!', error)
// })
